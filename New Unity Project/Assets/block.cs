﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class block : MonoBehaviour {

    public AudioClip saw;    

    void Start()
    {
        GetComponent<AudioSource>().playOnAwake = false;
        
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip);
        Destroy(gameObject);

    }
}
